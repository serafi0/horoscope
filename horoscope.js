function start() {
  let name = prompt("what's your name ?");
  console.log(name);
  while (!name) {
    name = prompt("what's your name ? please enter a valid name");
  }
  document.getElementById("username").textContent = `👋 hey there,${name}`;

  askForPassword();
}

function askForPassword() {
  let password = prompt("enter your password");
  let wrongPasswordCount = 0;
  while (password != "123") {
    ++wrongPasswordCount;
    if (wrongPasswordCount === 3) {
      alert("you submitted the wrong password 3 times!");
      break;
    }
    password = prompt("please enter your password again");
  }
  if (password == "123") {
    askForBirhday();
  }
}

function askForBirhday() {
  let birthMonth = Number(prompt("what's your birth month ?"));
  while (!Number.isInteger(birthMonth) || birthMonth > 12 || birthMonth < 1) {
    console.log("please enter a valid birth month nubmer  ie: from 1 to 12");
    birthMonth = Number(prompt("what's your birth month ?"));
  }

  let birthDay = Number(prompt("in what day where you born ?"));
  while (!birthDay || birthDay > 31 || birthDay < 0) {
    birthDay = Number(
      prompt("please enter a valid birth day ie: from 1 to 31")
    );
  }

  findAstroSign(birthDay, birthMonth);
}

function boundary(month, day) {
  return new Date(null, month, day);
}

function findAstroSign(day, month) {
  const date = new Date(null, month, day);
  if (!date) {
    alert("please enter a valid date");
    askForBirhday();
  }
  let zodiacSign;
  if (date >= boundary(3, 21) && date <= boundary(4, 19)) {
    zodiacSign = "Aries ♈";
  } else if (date >= boundary(4, 20) && date <= boundary(5, 20)) {
    zodiacSign = "Taurus ♉";
  } else if (date >= boundary(5, 21) && date <= boundary(6, 21)) {
    zodiacSign = "Gemini ♊";
  } else if (date >= boundary(6, 22) && date <= boundary(7, 22)) {
    zodiacSign = "Cancer ♋ ";
  } else if (date >= boundary(7, 23) && date <= boundary(8, 22)) {
    zodiacSign = "Leo ♌";
  } else if (date >= boundary(8, 23) && date <= boundary(9, 22)) {
    zodiacSign = "Virgo ♍";
  } else if (date >= boundary(9, 23) && date <= boundary(10, 23)) {
    zodiacSign = "Libra ♎";
  } else if (date >= boundary(10, 24) && date <= boundary(11, 21)) {
    zodiacSign = "Scorpius ♏";
  } else if (date >= boundary(11, 22) && date <= boundary(12, 21)) {
    zodiacSign = "Sagittarius ♐";
  } else if (date >= boundary(12, 22) && date <= boundary(1, 19)) {
    zodiacSign = "Sagittarius ♐";
  } else if (date >= boundary(11, 22) && date <= boundary(12, 21)) {
    zodiacSign = "Capricious ♑";
  } else if (date >= boundary(1, 20) && date <= boundary(2, 18)) {
    zodiacSign = "Aquarius ♒";
  } else if (date >= boundary(2, 19) && date <= boundary(3, 20)) {
    zodiacSign = "Pisces ♓ ";
  }
  document.getElementById(
    "zodiacSign"
  ).textContent += `it looks like your zodiac sign is ${zodiacSign}`;
  alert(`your sign is ${zodiacSign}`);
}
